# Maintainer: Jonathan Albrieux <jonathan.albrieux@gmail.com>
# This package is heavily inspired by the Alpine https://git.alpinelinux.org/aports/plain/community/retroarch/APKBUILD
pkgname=retroarch
pkgver=1.9.1
pkgrel=0
arch="all !mips !mips64"
url="https://retroarch.com"
pkgdesc="Reference frontend for the libretro API"
license="GPL-2.0-only"
depends="retroarch-assets retroarch-joypad-autoconfig libretro-core-info libretro-database"
makedepends="linux-headers mesa-dev qt5-qtbase-dev wayland-dev wayland-protocols zlib-dev alsa-lib-dev pulseaudio-dev sdl2-dev flac-dev mbedtls-dev libusb-dev ffmpeg-dev libxkbcommon-dev eudev-dev vulkan-loader-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/libretro/retroarch/archive/v$pkgver.tar.gz
	config.patch overlays.patch"
subpackages="$pkgname-doc"
builddir="$srcdir/RetroArch-$pkgver"
options="!check" # No tests

build() {
	CFLAGS="$CFLAGS -I/usr/include/directfb"
	./configure \
		--prefix=/usr \
		--enable-dynamic \
		--disable-vg \
		--disable-discord \
		--disable-builtinminiupnpc \
		--disable-builtinmbedtls \
		--disable-builtinflac \
		--disable-builtinzlib \
		--disable-git_version
	make
}

package() {
	DESTDIR="$pkgdir" make install
}

sha512sums="c0db73e287da3a3c5b73ae3e596b1bd8fe1d3d72326dda7a9090d7dd8eb8d92e1e76d2cae57b29035c26a498a2e9c03116033bf86c185d77a77feb5a49a6759d  retroarch-1.9.1.tar.gz
af09d804cf4c90cc46a33d6c5a4661be2baddc1e1682ba13b88054bb0fd9dad756b16006d731bdc9587ae4341f648a5c0131b936b44a916794d27cb7013b737d  config.patch
0e1c6f9932de8d5a8f50dd46b6831fc7b5660c4c1d1cfbf74e37841a7139586593a66c6a36720cbd1f62134df6323c9ed2295d55fccf48e57b6a19b623f63218  overlays.patch"
